#!/bin/bash

#Set version numbers
OLD_VERSION=$1
NEW_VERSION=$2

if [[ ! -z $OLD_VERSION && ! -z $NEW_VERSION  ]]; then
    echo "Releasing..."

    #Set version # at org.kde.glosbit.desktop
    SET_VERSION_CMD="sed -i 's/Version=$OLD_VERSION/Version=$NEW_VERSION/g' glosbit/org.kde.glosbit.desktop"
    GO_GLOSBIT="cd glosbit"
    ADD_VERSION_CHANGE="git add org.kde.glosbit.desktop"
    COMMIT_VERSION_CHANGE="git commit -m 'Updated version number'"
    PUSH_VERSIONING_COMMIT="git push"
    #Tag the new version
    GIT_TAG_CMD="git tag v$NEW_VERSION" 
    #Push tag
    GIT_PUSH_TAG_CMD="git push origin v$NEW_VERSION"
    GO_BACK="cd .."
    #Create atchive
    CREATE_ARCHIVE_CMD="tar --exclude='glosbit/CMakeLists.txt.user' --exclude='glosbit/.gitignore' --exclude='glosbit/glosbit-1.png' --exclude='glosbit/glosbit-2.png' --exclude='build*' --exclude='changelog' -cvf glosbit$NEW_VERSION.tar.gz glosbit/*"

    clear
    echo "*** Manual tasks ***"
    echo "Update changelog (from git log)"
    echo "Update README file if needed"
    echo
    echo "*** Execute the below for releasing*** "
    echo $SET_VERSION_CMD
    echo $GO_GLOSBIT
    echo $ADD_VERSION_CHANGE
    echo $COMMIT_VERSION_CHANGE
    echo $PUSH_VERSIONING_COMMIT
    echo $GIT_TAG_CMD
    echo $GIT_PUSH_TAG_CMD
    echo $GO_BACK
    echo $CREATE_ARCHIVE_CMD
fi

